require "core"

-- On Windows Powershell, need to call like so:
-- $env:NVIM_QT=1; nvim-qt; Remove-Item env:NVIM_QT
if os.getenv("NVIM_QT") then
  vim.g.nvim_qt = true
end

if vim.g.neovide or vim.g.nvim_qt then
  vim.g.fontname = (vim.g.neovide and "Agave Nerd Font" or "Consolas")
  vim.g.fontsize = 8
  vim.o.guifont = vim.g.fontname .. ":h" .. vim.g.fontsize
end

local custom_init_path = vim.api.nvim_get_runtime_file("lua/custom/init.lua", false)[1]

if custom_init_path then
  dofile(custom_init_path)
end

require("core.utils").load_mappings()

local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"

-- bootstrap lazy.nvim!
if not vim.loop.fs_stat(lazypath) then
  require("core.bootstrap").gen_chadrc_template()
  require("core.bootstrap").lazy(lazypath)
end

dofile(vim.g.base46_cache .. "defaults")
vim.opt.rtp:prepend(lazypath)
require "plugins"
