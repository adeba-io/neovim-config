local function AdjustFontSize(amount)
  vim.g.fontsize = vim.g.fontsize + amount
  vim.o.guifont = vim.g.fontname .. ":h" .. vim.g.fontsize
end

local function tree_grep()
	local path = ''
	local node = require('nvim-tree.lib').get_node_at_cursor()
	if node then
		path = node.absolute_path or uv.cwd()
		if node.type ~= 'directory' and node.parent then
			path = node.parent.absolute_path
		end
	else
		path = vim.api.nvim_buf_get_name(0)
		path = vim.fs.dirname(path)
	end
	require('telescope.builtin').live_grep({
		search_dirs = { path },
		prompt_title = string.format('Grep in [%s]', vim.fs.basename(path))
	})
end



---@type MappingsTable
local M = {}

M.general = {
  n = {
    [";"] = { ":", "enter command mode", opts = { nowait = true } },
    ["ZZ"] = { "<cmd> qa <CR>", "Quit Nvim" },
    ["<C-z>"] = { ":undo <CR>", "Undo" },

    --  format with conform
    ["<leader>fm"] = {
      function()
        require("conform").format()
      end,
      "formatting",
    },

    ["="] = {
      function()
        AdjustFontSize(1)
      end,
      "Increase Font Size"
    },
    ["-"] = {
      function()
        AdjustFontSize(-1)
      end,
      "Decrease Font Size"
    },

    ["<leader>cx"] = {
      function()
        require("nvchad.tabufline").closeAllBufs()
      end,
      "Close all buffers"
    },

    ["<leader>mh"] = {
      function()
        require("nvchad.tabufline").move_buf(-1)
      end,
      "Move Buffer Left"
    },
    ["<leader>ml"] = {
      function()
        require("nvchad.tabufline").move_buf(1)
      end,
      "Move Buffer Right"
    },

    ["<leader>fd"] = { function() tree_grep() end, "Find in Dir" }
  },
  i = {
    ["<C-z>"] = { ":undo <CR>", "Undo" },

    -- TODO Get this working
    -- switch between windows
    -- ["<C-h>"] = { "<C-w>h>", "Window left" },
    -- ["<C-l>"] = { "<C-w>l", "Window right" },
    -- ["<C-j>"] = { "<C-w>j", "Window down" },
    -- ["<C-k>"] = { "<C-w>k", "Window up" },
  },
  v = {
    [">"] = { ">gv", "indent"},
  },
}

-- more keybinds!

M.general.n["<leader>hd"] = {
  function()
    require 'hex'.dump()
  end,
  "Switch to Hex View"
}
M.general.n["<leader>ha"] = {
  function()
    require 'hex'.assemble()
  end,
  "Switch to Normal View"
}
M.general.n["<leader>ht"] = {
  function()
    require 'hex'.toggle()
  end,
  "Toggle Hex View"
}

return M
