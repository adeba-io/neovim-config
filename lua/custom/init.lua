-- local autocmd = vim.api.nvim_create_autocmd

-- Auto resize panes when resizing nvim window
-- autocmd("VimResized", {
--   pattern = "*",
--   command = "tabdo wincmd =",
-- })

if vim.g.neovide then
  vim.g.neovide_hide_mouse_when_typing = true
end

vim.o.colorcolumn = "90"

vim.opt.list = true
vim.opt.listchars = { tab = ">-", eol = "¬", trail ="~" }

vim.o.expandtab = false
